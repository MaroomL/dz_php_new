<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.05.18
 */

class RegisterController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('index');
    }

    public function loginAction()
    {
        $statusPass = isset($_SESSION['statusPass']) ? $_SESSION['statusPass'] : null;

        unset($_SESSION['statusPass']);
        return $this->render('login', [
            'statusPass' => $statusPass
        ]);
    }

    public function authAction(Request $request)
    {
        $model = User::findOneBy(['email' => $request->post('email')]);

        if ($model) {
             if (md5($request->post('password')) === $model->password) {
                 $_SESSION['user_id'] = $model->id;
                 Router::redirect('?route=book/index');
             }
             else {
                 $_SESSION['statusPass'] = 'Password or email is incorrect';
                 Router::redirect('?route=register/login');
             }
        }
        else {
            $_SESSION['statusPass'] = 'Password or email is incorrect';
            Router::redirect('?route=register/login');
        }
    }

    /**
     * @param Request $request
     */
    public function storeAction(Request $request)
    {
        $model = new User();
        $model->email = $request->post('email');
        $model->password = md5($request->post('password'));
        $model->name = $request->post('name');
        $model->ip = $_SERVER['REMOTE_ADDR'];

        $model->save();

        return Router::redirect('?route=book/index');
    }

    public function editAction(Request $request)
    {
        $model = User::find($request->get('id'));

        return $this->render('edit', compact('model'));
    }

    public function updateAction(Request $request)
    {
        /**
         * @var User $model
         */
        $model = User::find($request->get('id'));

        $model->name = $request->post('name');
        $model->email = $request->post('email');

        $model->save();
    }

    public function logoutAction() {
        unset($_SESSION['user_id']);
        Router::redirect('?route=book/index');
    }

//    public function default_layoutAction() {
//        $statusUID = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
//        if ($statusUID === '1') {
//            $regOrLog = 'Выйти';
//        }
//        else {
//            $regOrLog = 'Войти';
//        }
//
//        return $this->render('default_layout', [
//            'regOrLog' => $regOrLog
//        ]);
//    }
}