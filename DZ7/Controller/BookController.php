<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.04.18
 */

class BookController extends Controller
{
    public function createAction(Request $request)
    {
        if (isset($_SESSION['user_id'])) {
            return $this->render('create');
        }
        else {
            return $this->render('createDef');
        }
    }

    public function storeAction(Request $request)
    {
        if (!$request->isPost()) {
            return false;
        }


        $model = new Book();
        $model->title = $request->post('title');
        $model->author = $request->post('author');
        $model->year = $request->post('year');
        $model->user_id = $_SESSION['user_id'];
        $model->save();

        $_SESSION['status'] = 'Book ' . $request->post('title') . ' has been successfully stored';

        Router::redirect('?route=book/index');
    }

    public function indexAction()
    {
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;
        unset($_SESSION['status']);
        if ($_SESSION['user_id']) {
            return $this->render('index', [
                'books' => Book::findByPar($_SESSION['user_id']),
                'status' => $status
            ]);
        }
        else {
            return $this->render('indexDef');
        }
    }

    public function editAction() {
        return $this->render('edit');
    }

    public function updateAction(Request $request) {
        /**
         * @var Book $model
         */

        $model = Book::find($request->get('id'));
        $model->title = $request->post('title');
        $model->author = $request->post('author');
        $model->year = $request->post('year');
        $model->save();
        $_SESSION['status'] = 'Book with title ' . $request->post('title') . ' has been successfully updated';

        Router::redirect('?route=book/index');
    }

    public function deleteAction(Request $request) {
        $modelDel = new Book();
        $modelDel->delete((int)$_GET["id"]);
        $_SESSION['status'] = 'Book with title ' . $_GET["title"] . ' has been successfully deleted';

        Router::redirect('?route=book/index');
    }

    public function searchAction(Request $request) {
        $model = Book::findOneBy(['title' => $request->get('title')]);
        if ($model) {
            $res = 'Информация о книге: ' . '<br>' . 'Название: ' . $model->title . '<br>' . 'Автор: ' . $model->author . '<br>' . 'Год выпуска: ' . $model->year;
        }
        else {
            $res = 'Book was not found';
        }
        return $this->render('search', [
            'result' => $res,
        ]);
    }
}





