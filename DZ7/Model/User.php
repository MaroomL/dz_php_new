<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.05.18
 */

class User extends AbstractModel
{
    protected static $table = 'users';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $ip;
}